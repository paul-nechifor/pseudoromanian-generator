# Pseudoromanian generator

Text generator similar to Lorem Ipsum, but with Romanian letters.

## Usage

Install the dependencies:

    npm install

Build the generated code from CoffeeScript file:

    npm run build

Run the tests.

    npm test

## License

ISC
